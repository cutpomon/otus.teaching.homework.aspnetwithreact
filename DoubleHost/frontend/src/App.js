import './App.css';
import { useState, useEffect } from 'react';

function App() {
  const [response, setResponse] = useState([]);

  useEffect(() => {
    fetch("https://localhost:7160/WeatherForecast")
      .then(res => res.json())
      .then((result) => {
        setResponse(result);
      });
  }, []);
  
  return (
    <div className="App">
      <div>
        {response.map(item => 
          <div>Date: {item.date}, TemperatureCelsius: {item.temperatureC}</div>
        )}
      </div>
    </div>
  );
}

export default App;
